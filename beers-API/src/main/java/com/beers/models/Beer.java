package com.beers.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "beer")
public class Beer {
	
	// Class attributes
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	
	@NotNull(message = "The beer's name cannot be null.")
	@Size(min = 3, max = 128, message = "The beer's name must contain between 3 and 128 characters.")
	@Column(name = "name", unique = true)
	private String name;
	
	@NotNull(message = "The beer's description cannot be null.")
	@Size(min = 10, max = 128, message = "The beer's description must contain between 3 and 128 characters.")
	private String description;
	
	@NotNull(message = "The beer's price cannot be null.")
	@Min(0)
	private double price = 0.0;
	
	// Constructors
	
	public Beer() { }
	
	public Beer(String name, String description) {
		this.name = name;
		this.description = description;
	}

	// Getters
	
	public long getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public double getPrice() {
		return price;
	}
	
	// Setters

	public void setID(long iD) {
		ID = iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
