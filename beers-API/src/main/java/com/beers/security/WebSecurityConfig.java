package com.beers.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
		 
		 // Disable CSRF token requeriment for the technical test.
		 http.csrf().disable();
		 
		 // State the session creation property as STATELESS to follow REST protocol.
		 http
		    .authorizeRequests()
		    .antMatchers("/**").permitAll()
		    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		 // Enable XSS protection in headers to avoid XSS attacks.
		 http.headers().xssProtection();
		 
	 }

}