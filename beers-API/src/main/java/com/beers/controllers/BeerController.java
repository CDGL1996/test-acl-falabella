package com.beers.controllers;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.http.client.ClientProtocolException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.beers.models.Beer;
import com.beers.services.BeerService.BeerServices;
import com.beers.services.CurrencyService.CurrencyServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/beers")
public class BeerController {
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getBeers(HttpServletResponse response) {

		// Execute the service's method to find all the beers in the database.
        List<Beer> beerList = BeerServices.getBeers();
        // If no beers have been found, return no content HTTP code.
        if(beerList.size() == 0) {
            return new ResponseEntity<Object>(null, HttpStatus.NO_CONTENT);
        }
        // Otherwise, simply return all the data as JSON.
        else {
            return new ResponseEntity<Object>(beerList, HttpStatus.OK);
        }
        
    }
	
	 @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Object> getBeer(@PathVariable long id) {
		 
		 // The beer's id number must be at least 1 or higher.
		 if(id < 1) {
			 return new ResponseEntity<Object>("Invalid ID.", HttpStatus.BAD_REQUEST);
		 } else {
			 
			 // Look for beer based on its ID.
		     Beer beerFound = BeerServices.getBeer(id);
		     // If no beer has been found, return not found 404 HTTP code.
		     if(beerFound == null) {
		         return new ResponseEntity<Object>("No beer has been found with this ID.", HttpStatus.NOT_FOUND);
		     }
		     // Otherwise, simply return the data.
		     else {
		         return new ResponseEntity<Object>(beerFound, HttpStatus.OK);
		     }
			 
		 } 
		 
	 }
	 
	 @GetMapping(value = "/{id}/boxprice/{currency}", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Object> getBeerbox(@PathVariable long id, @PathVariable String currency) throws ClientProtocolException, IOException {
		 
		 // The beer's id number must be at least 1 or higher.
		 if(id < 1 || currency == null) {
			 return new ResponseEntity<Object>("Invalid ID or null currency.", HttpStatus.BAD_REQUEST);
		 } else {
			 
			 // Look for beer based on its ID.
		     Beer beerFound = BeerServices.getBeer(id);
		     // If no beer has been found, return not found 404 HTTP code.
		     if(beerFound == null) {
		         return new ResponseEntity<Object>("No beer has been found with this ID.", HttpStatus.NOT_FOUND);
		     }
		     // Otherwise, simply return the data.
		     else {
		    	 	
		    	 // Store the request's currency at a variable.
		    	 String requestCurrency = currency.toUpperCase();
		    	 // Get the requested beer's price.
		    	 double beerPrice = beerFound.getPrice();
		    	 // Load the list of all the currencies.
		    	 JsonObject currencyList = CurrencyServices.getCurrencies();
		    	 // Get the value of the specific currency set by the API request.
		    	 JsonElement currencyValue = currencyList.get(requestCurrency);
		    	 // No currency with that name has been found, return error.
		    	 if(currencyValue == null) {
		    		 return new ResponseEntity<Object>("Invalid currency.", HttpStatus.BAD_REQUEST);
		    	 } else {
		    		 double value = currencyValue.getAsDouble();
			    	 // Do the calculation to find the value.
			    	 double beerboxValue = (beerPrice * value) * 6;
			    	 
			    	 // Format the output which will be shown as a response.
			    	 Gson gson = new GsonBuilder().setPrettyPrinting().create();
			    	 JsonElement jsonElement = gson.toJsonTree(beerFound);
			         JsonObject formattedOutput = new JsonObject();
			         formattedOutput.add("beer", jsonElement);
			         
			         // Display only two decimals.
			         DecimalFormat twoDecimals = new DecimalFormat("#,###,###,##0.00");
			         formattedOutput.addProperty("beer box's price", twoDecimals.format(beerboxValue));
			         String jsonResponse = gson.toJson(formattedOutput);
			    	 
			         // Finally, return the response with a 200 status code, OK.
			         return new ResponseEntity<Object>(jsonResponse, HttpStatus.OK);
		    	 } 
		     }
			 
		 } 
		 
	 }
	 
	 @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Object> createBeer(@RequestBody @Valid Beer beer,
	                                                            HttpServletRequest request, HttpServletResponse response) {

		// Check if there's already a beer with this name.
	    if(BeerServices.getBeer(beer.getName()) != null) {
	    	// If there is, return an error for the conflicting name.
	        return new ResponseEntity<Object>("There's already a beer with this name.", HttpStatus.CONFLICT);
	    }
	    else {
	    	// Otherwise normally proceed.
	        Beer createdBeer = BeerServices.createBeer(beer);
	        // Add a header with the URL for the newly created object.
	        response.addHeader("location", request.getRequestURL().toString() + "/" +createdBeer.getID());
	        return new ResponseEntity<Object>(createdBeer, HttpStatus.CREATED);
	    }
	    
	}
	 
	 @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Object> updateBeer(@PathVariable long id, @RequestBody @Valid Beer beer,
	                                                    HttpServletResponse response) {

		// Check if there's actually a beer with this ID.
	    Beer foundBeer = BeerServices.getBeer(id);
	    // If there isn't return not found error.
	    if(foundBeer == null) {
	        return new ResponseEntity<Object>("There's no beer with this ID.", HttpStatus.NOT_FOUND);
	    }
	    // Look if the new name conflicts with another beer's name in the database.
	    Beer foundName = BeerServices.getBeer(beer.getName());
	    // If it does, return an error of conflict.
	    if(foundName != null && id != foundName.getID()) {
	        return new ResponseEntity<Object>("There's already another beer with this name.", HttpStatus.CONFLICT);
	    }
	    else {
	    	// If everything is fine, execute the service and update the existing beer.
	        Beer updatedBeer = BeerServices.updateBeer(id, beer);
	        return new ResponseEntity<Object>(updatedBeer, HttpStatus.OK);
	    }
	}
	 
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteBeer(@PathVariable long id,
	                                                    HttpServletResponse response) {

		// If there's no beer with this ID, return a 404 not found error.
	    if(BeerServices.getBeer(id) == null) {
	        return new ResponseEntity<Object>("There's no beer with this ID.", HttpStatus.NOT_FOUND);
	    }
	    else {
	    	// Otherwise, proceed by executing the service and deleting the item.
	        Beer deletedBeer = BeerServices.deleteBeer(id);
	        return new ResponseEntity<Object>(deletedBeer, HttpStatus.OK);
	    }
	    
	}

}
