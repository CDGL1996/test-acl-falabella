package com.beers.services;

import java.io.IOException;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CurrencyService {
	
	public static class CurrencyServices {
		
		public static JsonObject getCurrencies() throws ClientProtocolException, IOException {
			
			//Creating a HttpClient object
		    CloseableHttpClient httpclient = HttpClients.createDefault();

		    //Creating a HttpGet object
		    HttpGet currencyURL = new HttpGet("http://www.apilayer.net/api/live?access_key=749157c2a8cdc8c90f5d290b3484f44a");

		    //Executing the Get request
		    HttpResponse httpresponse = httpclient.execute(currencyURL);

		    Scanner sc = new Scanner(httpresponse.getEntity().getContent());
		    // Extract only the values from the response using sub-strings.
		    String extractedResponse = null;
		    int valuesPosition = 0;
		    String currencyValues = null;

		    while(sc.hasNext()) {
		    	extractedResponse = sc.nextLine();
		    }
		    sc.close();
		    // Extracting unnecessary info from the API response.
		    valuesPosition = extractedResponse.indexOf("quotes");
		    // Keeping only currency values and keys.
	    	currencyValues = extractedResponse.substring(valuesPosition + 8, extractedResponse.length() - 1);
	    	// Transforming the data into JSON again.
	    	Gson gson = new Gson();
	    	JsonElement element = gson.fromJson(currencyValues, JsonElement.class);
	    	// Creating a final currency JSON object to return all the data formatted.
	    	JsonObject currencyJSON = element.getAsJsonObject();
	    	System.out.println("Currency Values:" + currencyJSON);
		    return currencyJSON;
		}
		
	}

}
