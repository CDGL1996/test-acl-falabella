package com.beers.services;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beers.models.Beer;

// Class created to handle the Hibernate ORM Framework config.
public class HibernateSession {
	
	public SessionFactory factory = configSession();
	
	public SessionFactory configSession() {
		SessionFactory fact;
		try {
	         fact = new Configuration().
	                   configure().
	                   addAnnotatedClass(Beer.class).
	                   buildSessionFactory();
	      } catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
		return fact;
	}
	
}