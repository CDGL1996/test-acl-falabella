package com.beers.services;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.beers.models.Beer;

public class BeerService {
	
	public static class BeerServices {
		
		// Get all beers.
		public static List<Beer> getBeers() {
			
			// List where the data will be stored.
			List<Beer> beerList = null;
			// Variable to handle hibernate's session
			HibernateSession factory = new HibernateSession();
            SessionFactory sesion = factory.configSession();

            Session session = sesion.openSession();
            Transaction tx = null;

            try {

                tx = session.beginTransaction();
                @SuppressWarnings("unchecked")
				Query<Beer> query = session.createQuery("FROM Beer");
                beerList = query.list();

            } catch (HibernateException e) {
            	// In case an error happens, rollback.
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
            } finally {
            	// Close the session, no matter what happens.
                session.close();
            }
            // Return the beer list with the data.
            return beerList;
			
		}
		
		// Get a single beer.
		public static Beer getBeer(long beerID) {
			
			// Variable where the found data will be stored.
			Beer foundBeer = new Beer();

			// Variables to handle hibernate's config.
            HibernateSession factory = new HibernateSession();
            SessionFactory sesion = factory.configSession();

            // Opening session.
            Session session = sesion.openSession();
            Transaction tx = null;

            try {
            	// Starting the transaction.
                tx = session.beginTransaction();
                // Looking for a beer based on its ID. (Primary Key)
                foundBeer = (Beer) session.get(Beer.class, beerID);

            } catch (HibernateException e) {
            	// In case of error, rollback.
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
            } finally {
            	// ALWAYS close session.
                session.close();
            }
            // Return the data found.
            return foundBeer;
		}
		
		// Get a single beer based on its name.
		public static Beer getBeer(String name) {
				
			// Object to store the found's beer data.
			Beer foundBeer = new Beer();

			// Default hibernate's config variables.
            HibernateSession factory = new HibernateSession();
            SessionFactory sesion = factory.configSession();

            // Opening a new session.
            Session session = sesion.openSession();
            Transaction tx = null;

            try {
            	// Search for the beer based on its name.
                tx = session.beginTransaction();
                @SuppressWarnings("rawtypes")
				Query query = session.createQuery("FROM Beer WHERE UPPER(name) = :name");
                query.setParameter("name", name.toUpperCase().trim());
                query.setMaxResults(1);
                foundBeer = (Beer) query.uniqueResult();

            } catch (HibernateException e) {
            	// In case of error, do a rollback.
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
            } finally {
            	// Shutting down session.
                session.close();
            }
            return foundBeer;
		}
	
		// Create a new beer.
		public static Beer createBeer(Beer newBeer) {
		
			// The newest created beer will be stored in this object.
			Beer createdBeer = new Beer();

			// Variables to handle hibernate's config.
			HibernateSession factory = new HibernateSession();
			SessionFactory sesion = factory.configSession();

			// Open a new session.
			Session session = sesion.openSession();
			Transaction tx = null;

			try {
				// Begin the transaction.
				tx = session.beginTransaction();
				// Save the new beer in the database.
				session.save(newBeer);
				// Commit changes.
				tx.commit();
				// Obtain the latest beer from the database by its ID in descending order.
				@SuppressWarnings("rawtypes")
				Query query = session.createQuery("FROM Beer order by ID DESC");
				// Limit the query to a single result.
				query.setMaxResults(1);
				createdBeer = (Beer) query.uniqueResult();

			} catch (HibernateException e) {
				// In case of error, in order to protect the data's integrity, rollback.
				if (tx != null) {
					tx.rollback();
				}
				e.printStackTrace();
			} finally {
				// ALWAYS shut down the session for good measure.
				session.close();
			}
			// Return the latest beer created.
			return createdBeer;
		}
	
		// Update an existing beer.
		public static Beer updateBeer(long beerID, Beer newBeer) {
		
			// Object where the updated Beer object will store its data.
			Beer updatedBeer = new Beer();

			// Default hibernate config variables.
			HibernateSession factory = new HibernateSession();
			SessionFactory sesion = factory.configSession();

			// Opening a new session.
			Session session = sesion.openSession();
			Transaction tx = null;

			try {
				tx = session.beginTransaction();
				// Loading the beer's data based on its ID.
				updatedBeer = (Beer) session.get(Beer.class, beerID);
				// Updating the old object with the new object's data.
				updatedBeer.setName(newBeer.getName());
				updatedBeer.setDescription(newBeer.getDescription());
				updatedBeer.setPrice(newBeer.getPrice());
				// Updating and committing changes.
				session.update(updatedBeer);
				tx.commit();

			} catch (HibernateException e) {
				// In case of error, rollback.
				if (tx != null) {
					tx.rollback();
				}
				e.printStackTrace();
			} finally {
				// ALWAYS close session.
				session.close();
			}
			// Return the object with the updated information. 
			return updatedBeer;	
		}
	
		// Delete an existing beer.
		public static Beer deleteBeer(long beerID) {
		
			// Variable where the data will be stored.
			Beer eliminatedBeer = new Beer();

			// Default hibernate config variables.
			HibernateSession factory = new HibernateSession();
			SessionFactory sesion = factory.configSession();

			// Opening a new session.
			Session session = sesion.openSession();
			Transaction tx = null;

			try {
				tx = session.beginTransaction();
				// Storing the data of the beer about to be deleted.
				eliminatedBeer = (Beer) session.get(Beer.class, beerID);
				// Remove the beer from the database.
				session.delete(eliminatedBeer);
				// Commit changes.
				tx.commit();

			} catch (HibernateException e) {
				// If any errors happen, do a rollback to protect the database's integrity.
				if (tx != null) {
					tx.rollback();
				}
				e.printStackTrace();
			} finally {
				// ALWAYS shut down the session.
				session.close();
			}
			// Return the data of the deleted object.
			return eliminatedBeer;
		}

	}
}
